#!/bin/bash
#
# Script for atomating camx2csv runs
# Center for Atmospheric Particle Studies
# Carnegie Mellon University
#
# Author:
#	Pablo GarcIa
#

#	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
# Functions
#	Function for displaying help
display_help ()
{
	echo "Script for atomating camx2csv runs"
	echo "Converts CAMx CF inventories into PMCAMx CMU-VBS"
	echo "Center for Atmospheric Particle Studies"
	echo "Carnegie Mellon University"
	echo "Syntax:"
	echo "	-d YYYY MM DD o --date YYYY MM DD"
	echo "		Required. Start date"
	echo "	-b o --debug"
	echo "		Optional. Run step by step"
	echo "		Default: FALSE"
	echo "	-s o --scenario"
	echo "		Optional. camx2csv configuration scenario"
	echo "			Associated to a camx2csv.sh template"
	echo "		Default: 12EUS_area"
	echo "	-f o --forecast"
	echo "		Optional. Number of days to run"
	echo "		Default: 1"
	echo "	-h o -help"
	echo "		Optional. Displays this help text"
	exit
}
get_date ()
{
# 	Function for producing a date component vector
# 	The output variable ${fvdate[@]} contains the following elements
#		[0] = Year in YY format
#		[1] = Month in MM format
#		[2] = Day in DD format
#		[3] = Day of the year in JJJ format
#		[4] = Year in YYYY format
#		[5] = Day of the week in U[1-7] format

#	Variable capture
#	year
	local ftyear=$1
#	month
	local ftmonth=$2
#	day
	local ftday=$3
#	offset
	local fddays=$4

#	Seconds from the epoch
	local ftepoch=`date --date=$ftyear"-"$ftmonth"-"$ftday +%s`
#	Offseted seconds from the epoch
	local fepoch=$(($ftepoch+$fddays*86400))

#	Date
	local fdate=`date --date="1970-01-01 "$fepoch" sec" +"%y-%m-%d-%j-%Y-%u"`

#	Date string parsing
#	The fvdate variable is global since its used to return data from the function
	fvdate=(`echo $fdate | tr '-' '\n'`)
#		${fvdate[0]}		Year in YY format
#		${fvdate[1]}		Month in MM format
#		${fvdate[2]}		Day in DD format
#		${fvdate[3]}		Day of the year in JJJ format
#		${fvdate[4]}		Year in YYYY format
#		${fvdate[5]}		Day of the week in U[1-7] format
}

pause_message ()
{
#	Function for displaying a state message and allow for continuing or terminating
	local fpause=$1
	local msg=$2
	if [ "$pause" = "-debug" ]; then
		echo $msg
		read -p "Continue (y/n)"
		if [ "$REPLY" != "y" ]; then
			echo "Stopped"
			exit
		fi
	fi
}

#	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
#					A	R	G	U	M	E	N	T	S
#	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*

# Argument capture for type -?
if [ $# -ge 4 ] # At least 4 arguments are necessary
	then
		# Loop the arguments
		while [ $# -ge 1 ]
			do
			# Argument selector, shift offsets the arguments
			case $1 in
				-d  | --date ) tyear=$2; tmonth=$3; tday=$4; shift 4;;
				-b  | --debug ) pause="-debug"; shift;;
				-s  | --scenario ) scenario=$2; shift 2;;
				-f  | --forecast ) forecast=$2; shift 2;;
				-h  | --help ) display_help; shift;;
				*) echo "Non valid argument"; exit;;
			esac
		done
else
	display_help
fi

# Check the date, does it exist and is a number?
if [ -z $tyear ] || [ -z $tmonth ] || [ -z $tday ]
	then
	echo "Not a valid date"
	exit
fi

# Default $scenario value
if [ -z $scenario ]
	then
	scenario="12EUS"
fi

# Default $forecast value
if [ -z $forecast ]
	then
	forecast=1
fi

#	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
#				c	a	m	x	z	e	x	t	r	a	c	t
#	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*

# Loop through the forcast days
for i in $(eval echo {1..$forecast})
do
	# Today
	get_date $tyear $tmonth $tday $((i-1))
	declare -a tvdate=(${fvdate[@]})

	# Build the filenames from the scenario file
	source $scenario.sh

	echo $in_file
	echo $out_file

	# debug
	pause_message $pause "Process emission files?"
	./camx2csv << EOF
$in_file
40. -97. 45. 33.
$out_file
EOF

done
