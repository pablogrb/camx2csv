	PROGRAM camx2csv

	USE csv_file
	USE class_uam_iv

	IMPLICIT NONE
!	------------------------------------------------------------------------------------------
!	Purpose:
!		Converts CAMx binary files into csv
!	Inputs:
!		CAMx file to be processed
!	Outputs:
!		Gas and particulate species totals per day
!	By:
!		Pablo Garcia
!		04-2016
!		Based on s_after_SAPRC and bin2asc

!	------------------------------------------------------------------------------------------
!	Declarations

!	Parameters
!	IO
	CHARACTER(LEN=256) :: out_file				! User IO for filenames
	TYPE(UAM_IV) :: fl								! Input average file

!	Geodata
	REAL :: lat, lon							! Current latitude and longitude
	REAL :: lamb_x, lamb_y						! Lambert projection coordinate in km
	REAL :: phic								! Central latitude (deg, neg for southern hem)
	REAL ::	xlonc								! Central longitude (deg, neg for western hem)
	REAL ::	truelat1							! First true latitute (deg, neg for southern hem) may be hts
	REAL ::	truelat2							! Second true latitute (deg, neg for southern hem) may be htl

!	Variables
	LOGICAL :: csv_record_end					! Logical marker of end of record

!	Counters
	INTEGER i_x,i_y,i_z,i_sp
	INTEGER pct_10								! 10% counter

!	------------------------------------------------------------------------------------------
!	Entry point
!	------------------------------------------------------------------------------------------
!
!	Read filenames and open files
!
	WRITE(*,*) 'Convert UAM-IV format CAMx files from binary to csv'
	WRITE(*,*) 'The following file types are recognized:'
	WRITE(*,*) ' AVERAGE'
	WRITE(*,*) ' AIRQUALITY'
! 	WRITE(*,*) ' BOUNDARY'
	WRITE(*,*) ' EMISSIONS'
! 	WRITE(*,*) ' PTSOURCE'
! 	WRITE(*,*) ' TEMPERATUR'
! 	WRITE(*,*) ' WIND'
	WRITE(*,*)
	WRITE(*,*) 'The file type will be determined automatically'
	WRITE(*,*)

	WRITE(*,*) 'Input file name: '
	READ (*,'(a)') fl%in_file
! 	Set the input file unit
	fl%unit = 10
! 	Read the input file
	CALL read_uamfile(fl)

	WRITE(*,*) 'Projection Parameters'
	READ (*,*) phic, xlonc, truelat1, truelat2

	WRITE(*,*) 'Output file name: '
	READ (*,'(a)') out_file
	OPEN (11,FILE=TRIM(out_file),STATUS='NEW')
	WRITE(*,*) 'Opened file: ',TRIM(out_file)

!	------------------------------------------------------------------------------------------
!	Write the csv

!	Header
	CALL csv_write (11,'cell x',.FALSE.)
	CALL csv_write (11,'cell y',.FALSE.)
	CALL csv_write (11,'cell z',.FALSE.)
	CALL csv_write (11,'lamb_x',.FALSE.)
	CALL csv_write (11,'lamb_y',.FALSE.)
	CALL csv_write (11,'lat',.FALSE.)
	CALL csv_write (11,'lon',.FALSE.)
	CALL csv_write (11,'date',.FALSE.)
	DO i_sp=1,fl%nspec
		IF (i_sp.LT.fl%nspec) THEN
			csv_record_end=.FALSE.
		ELSE
			csv_record_end=.TRUE.
		END IF
		CALL csv_write (11,fl%c_spname(i_sp),csv_record_end)
	END DO

	pct_10 = 1+fl%nx/10
	WRITE(*,*) 0, "% done"

!	Loop through the cells
	DO i_x = 1,fl%nx
		DO i_y = 1,fl%ny
			DO i_z = 1,fl%nz
! 				Cell numbers
				CALL csv_write (11,i_x,.FALSE.)
				CALL csv_write (11,i_y,.FALSE.)
				CALL csv_write (11,i_z,.FALSE.)
!				Calculate cell position in km
				lamb_x = (fl%utmx + REAL(i_x-1)*fl%dx)/1000
				lamb_y = (fl%utmy + REAL(i_y-1)*fl%dy)/1000
				CALL csv_write (11,lamb_x,.FALSE.)
				CALL csv_write (11,lamb_y,.FALSE.)
! 				Get lat lon
				CALL lcpgeo(1,phic,xlonc,truelat1,truelat2,lamb_x,lamb_y,lon,lat)
! 				Write lat and lon
				CALL csv_write (11,lat,.FALSE.)
				CALL csv_write (11,lon,.FALSE.)
! 				Write the date
				CALL csv_write (11,fl%idate,.FALSE.)

! 				Write species concentrations
				DO i_sp = 1,fl%nspec
					IF (i_sp.LT.fl%nspec) THEN
						csv_record_end=.FALSE.
					ELSE
						csv_record_end=.TRUE.
					END IF
! 					Get concentrations or emissions according to file type
					SELECT CASE (fl%ftype)
					CASE ('AVERAGE   ','AIRQUALITY')
						CALL csv_write (11,SUM(fl%conc(i_x,i_y,i_z,:,i_sp))/fl%update_times,&
							&csv_record_end)
					CASE ('EMISSIONS ')
						CALL csv_write (11,SUM(fl%aemis(i_x,i_y,:,i_sp)),&
							&csv_record_end)
					CASE DEFAULT
						WRITE(*,*) 'class_UAM_IV error: Not a valid file type'
						CALL EXIT(0)
					END SELECT
				END DO
			END DO
		END DO

		IF (MOD(i_x,pct_10) .EQ. 0) THEN
			WRITE(*,*) 100*i_x/fl%nx, "% done"
		END IF
	END DO
	WRITE(*,*) 100, "% done"
!  	Close the csv
	CLOSE(11)

END PROGRAM camx2csv
