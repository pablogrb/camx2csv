#!/bin/bash
rm csv_file.mod
rm camx2csv
ifort class_uam_iv.f90 csv_file.f90 lcpgeo.f camx2csv.f90 -o camx2csv -g -mieee-fp -align dcommons -convert big_endian -static_intel -extend-source

# rm area.camx.20130712.csv
# ./camx2csv << EOF
# area.camx.20130712.bin
# area.camx.20130712.csv
# EOF

rm test_run-20130728.csv
./camx2csv << EOF
test_run-20130728.avrg
40. -97. 45. 33.
test_run-20130728.csv
EOF
